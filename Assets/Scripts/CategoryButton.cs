﻿using UnityEngine;
using UnityEngine.UI;

public class CategoryButton : MonoBehaviour
{
   
    public StartGameController controller;
    public Button button;
    public Text categoryName;
    public UIController uIController;
    public Text passedQuizes;
    private void OnEnable()
    {
        button = GetComponent<Button>();
    }
    public void StartGame()
    {
        controller.Init(categoryName.text);
        uIController.OpenWindow("startgamepanel");
    }
}