﻿using Newtonsoft.Json;
using System;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    public bool musicTumbler;
    public bool soundTumbler;
    public bool notificationTumbler;
    public Toggle musicToggle;
    public Toggle soundToggle;
    public Toggle notificationToggle;
    void Start()
    {
        LoadData();
        SetValues();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void SetValues()
    {
        musicToggle.isOn = musicTumbler;
        soundToggle.isOn = soundTumbler;
        notificationToggle.isOn = notificationTumbler;
    }
    public void SetMusicState(bool value)
    {
        musicTumbler = value;
    }
    public void SetSoundState(bool value)
    {
        soundTumbler = value;
    }

    public void SetNotificationState(bool value)
    {
        notificationTumbler = value;
    }
    private void OnApplicationQuit()
    {
        SaveData();
    }
    private void OnApplicationPause(bool pause)
    {
        if (pause)
            SaveData();
    }
    #region SaveDataMethods
    private void SaveData()
    {
        SetingsData data = new SetingsData(this);
        string json = JsonUtility.ToJson(data);
        PlayerPrefs.SetString("Settings", json);
        Debug.Log("Данные сохранены" + PlayerPrefs.GetString("Settings"));
    }
    private void LoadData()
    {
        if (PlayerPrefs.HasKey("Settings"))
        {
            string json = PlayerPrefs.GetString("Settings");
            SetingsData data = JsonConvert.DeserializeObject<SetingsData>(json);
            data.SetData(this);
            Debug.Log("Данные настроек загружены");
        }

    }
    #endregion
}
[Serializable]
public class SetingsData
{
    public bool musicTumbler;
    public bool soundTumbler;
    public bool notificationTumbler;

    public SetingsData()
    {
    }

    public SetingsData(SettingsController settingsController)
    {
        musicTumbler = settingsController.musicTumbler;
        soundTumbler = settingsController.soundTumbler;
        notificationTumbler = settingsController.notificationTumbler;
    }
    public void SetData(SettingsController settingsController)
    {
        settingsController.musicTumbler = musicTumbler;
        settingsController.soundTumbler = soundTumbler;
        settingsController.notificationTumbler = notificationTumbler;
    }
}