﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingBehevior : MonoBehaviour
{
    [SerializeField]
    Animator loadingPanelAnimator;
    [SerializeField]
    Animator mainMenuAnimator;
    UIController uIController;
    TimeSystem timeSystem;
    [SerializeField]
    Text loadingText;
    [SerializeField]
    Image loadingBar;
    private void Awake()
    {
        if (!PlayerPrefs.HasKey("FirstStart"))
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetString("FirstStart", "s");
        }
    }
    void Start()
    {
        uIController = GetComponent<UIController>();
        timeSystem = GetComponent<TimeSystem>();
        StartCoroutine(Loading());
    }
    IEnumerator Loading()
    {
        
        yield return new WaitForSeconds(2f);
        Debug.Log("Запустил проверку");
        yield return timeSystem.StartCoroutine(timeSystem.CheckOnline());
        Debug.Log("Закончил проверку");
        yield return new WaitForSeconds(0.1f);
        if (timeSystem.isTrueConnected)
        {
            loadingPanelAnimator.SetBool("end", true);
            CanvasGroup canvasGroup = loadingPanelAnimator.gameObject.GetComponent<CanvasGroup>();
            uIController.OpenWindow("menuwindows");
            yield return new WaitWhile(() => canvasGroup.alpha != 0);
            loadingPanelAnimator.gameObject.SetActive(false);
        }
        else
        {
            loadingText.text = "Отсутствует интернет соединение".ToUpper();
            loadingBar.gameObject.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
