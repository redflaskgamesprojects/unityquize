﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public class GameScript : MonoBehaviour
{
    #region Variables
    public PlayingCategory currentCategory;
    public Quize currentQuize;
    [Header("Панель окончания уровня")]
    [SerializeField]
    EndGamePanel endGamePanel;
    [Header("Игровая панель")]
    [SerializeField]
    private Text _quizText;
    [SerializeField]
    private Text[] _answersText;
    [SerializeField]
    private Image TimerProgress;
    [SerializeField]
    private Text TimerText;
    [SerializeField]
    private Animation TextAnimation;
    [SerializeField]
    private Animation[] ButtonAnimations;
    public Button[] Buttons;
    [Header("Другие панели")]
    [SerializeField]
    private List<Button> hints;
    //
    private AdManager adManager;
    private bool startGame;
    public float Time = 25;
    private int _Points;
    public bool GetAnswer;
    private string answertext;
    private ResourceManager resourceManager;
    ChangeGameScript changeGameScript;
    StoryModeController storyModeController;
    private int time = 25;
    private Color currentColor = new Color(0.1877889f, 0.6037736f, 0.08410466f, 1);
    private bool iteration, animated, paused = false;
    private float pausedTime;
    private int failedAnswer;
    private float remainingTime;
    private bool filedFlag;
    UIController uIController;
    public bool doubleTry;
    public bool isStoryGame;
    #endregion

    void Start()
    {
        storyModeController = GetComponent<StoryModeController>();
        uIController = GetComponent<UIController>();
        changeGameScript = GetComponent<ChangeGameScript>();
        resourceManager = GetComponent<ResourceManager>();
        adManager = GetComponent<AdManager>();
    }
    IEnumerator Game()
    {
        while (resourceManager.life > 0)
        {
            iteration = true;
            GameConroller();
            StopCoroutine("Timer");
            Time = 20;
            yield return new WaitWhile(() => paused != false);
            yield return new WaitForSeconds(3f);
            if (paused == false)
                StartCoroutine("Timer");
            yield return new WaitWhile(() => GetAnswer != true);
            if (resourceManager.life <= 0)
            {
                Debug.Log("Жизни закончились");
                EndGame(1);
                iteration = false;
                yield break;
            }
            else if (currentCategory.quizes.Count < 1)
            {
                Debug.Log("Закончились вопросы");
                if (isStoryGame && _Points >= 2)
                    EndGame(0);
                else if (isStoryGame && _Points < 2)
                    EndGame(4);
                else
                    EndGame(3);
                iteration = false;
                yield break;
            }
            yield return new WaitWhile(() => animated == true);
            GetAnswer = false;
            iteration = false;
        }
    }
    IEnumerator Timer()
    {
        while (Time != 0)
        {
            Time--;
            yield return new WaitForSeconds(1);
        }
    }

    private void GameConroller()
    {
        _quizText.text = "";
        if (!paused)
        {
            float range = UnityEngine.Random.Range(0, 1000);
            int currentQuizeIndex = (int)Mathf.Round(range / (1000 / currentCategory.quizes.Count));
            currentQuize = currentCategory.quizes[currentQuizeIndex % currentCategory.quizes.Count];
        }
        _quizText.text = currentQuize.quizeText;
        TextAnimation.Play("QuizeTextAnim");
        for (int i = 0; i < currentQuize.answers.Count; i++)
        {
            _answersText[i].text = currentQuize.answers[i].answerText;
        }
        for (int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].gameObject.GetComponent<Animation>().Play(string.Format("StandartState"));
        }
        ShuffleButtons();
    }
    private void ShuffleButtons()
    {
        Vector2[] localPosition;
        localPosition = new Vector2[Buttons.Length];
        for (int i = 0; i < Buttons.Length; i++)
        {
            localPosition[i].x = Buttons[i].gameObject.transform.localPosition.x;
            localPosition[i].y = Buttons[i].gameObject.transform.localPosition.y;
        }
        System.Random rand = new System.Random();
        for (int i = localPosition.Length - 1; i >= 1; i--)
        {
            int j = rand.Next(i + 1);

            Vector2 tmp = localPosition[j];
            localPosition[j] = localPosition[i];
            localPosition[i] = tmp;
        }
        for (int i = 0; i < localPosition.Length; i++)
        {
            Buttons[i].gameObject.transform.localPosition = localPosition[i];
        }
    }

    #region Events
    public void Pause()
    {
        pausedTime = Time;
        StopCoroutine("Timer");
        paused = true;
    }
    public void Countine()
    {
        Time = pausedTime;
        paused = false;
        StartCoroutine("Timer");
    }
    IEnumerator FiledWait()
    {
        filedFlag = true;
        Filed();
        yield return new WaitForSeconds(0.1f);
        filedFlag = false;
    }
    private void Filed()
    {
        resourceManager.life--;
        Debug.Log("Минус жизнь");
    }
    public void StartGame()
    {
        startGame = true;
        _quizText.text = "";
    }
    public void RestartAdGame()
    {
        adManager.ShowRewarded();
        adManager._RewardedLife.OnUserEarnedReward += _RewardedLife_OnUserEarnedReward;
    }
    private void _RewardedLife_OnUserEarnedReward(object sender, GoogleMobileAds.Api.Reward e)
    {
        resourceManager.life += 2;
        uIController.CloseWindow("endgamewindow");

        StartGame();
        Pause();
    }
    public void EndGame(int id)
    {
        int bonusadd = (int)Math.Truncate(((remainingTime * 0.2d) + (_Points * 5)) * 0.1);
        int timeadd = (int)Math.Truncate(remainingTime * 0.1);
        int pointsadd = _Points * 8;
        int allAdd = bonusadd + timeadd + pointsadd;
        int expAdd = _Points * 2;
        if (id != 1)
        {
            if (failedAnswer > 0)
                adManager.ShowAdPage();
        }
        if (isStoryGame)
        {

            int count = _Points;
            foreach (var item in endGamePanel.stars)
            {
                if (count > 0)
                    item.sprite = endGamePanel.fullStarSprite;
                else
                    item.sprite = endGamePanel.nullStarSprite;
                count--;
            }


        }

        endGamePanel.adButton.gameObject.SetActive(false);
        endGamePanel.nextLevelButton.gameObject.SetActive(false);
        endGamePanel.exitButton.gameObject.SetActive(false);
        StopCoroutine("Timer");
        StopCoroutine("Game");
        startGame = false;
        paused = false;
        iteration = false;
        GetAnswer = false;
        isStoryGame = false;
        doubleTry = false;
        endGamePanel.moneyText.text = string.Format("{0}", allAdd);
        endGamePanel.experienceText.text = string.Format("+{0}XP", expAdd);
        resourceManager.money += allAdd;
        if (storyModeController.currentLevel == storyModeController.maxLevel && isStoryGame)
            id = 2;
        // 0 - Уровень пройден,1-Закончились жизни, 2-Игра завершена 3- Вопросы закончились
        switch (id)
        {
            case 0:
                endGamePanel.nextLevelButton.gameObject.SetActive(true);//р
                endGamePanel.stateText.text = "Уровень пройден".ToUpper();//р
                storyModeController.OnLevelEnd.Invoke(currentCategory.name);
                break;
            case 1:
                endGamePanel.adButton.gameObject.SetActive(true);//р
                endGamePanel.stateText.text = "Здоровье закончилось".ToUpper();//р
                break;
            case 2:
                endGamePanel.exitButton.gameObject.SetActive(true);//р
                endGamePanel.menuButton.gameObject.SetActive(false);
                endGamePanel.settingsButton.gameObject.SetActive(false);
                endGamePanel.stateText.text = "Игра завершена".ToUpper();//р
                break;
            case 3:
                endGamePanel.exitButton.gameObject.SetActive(true);//р
                endGamePanel.menuButton.gameObject.SetActive(false);
                endGamePanel.settingsButton.gameObject.SetActive(false);
                endGamePanel.stateText.text = "Вопросы закончились".ToUpper();//р
                break;
            case 4:
                endGamePanel.exitButton.gameObject.SetActive(true);//р
                endGamePanel.menuButton.gameObject.SetActive(false);
                endGamePanel.settingsButton.gameObject.SetActive(false);
                endGamePanel.stateText.text = "Уровень не пройден".ToUpper();//р
                break;
        }
        uIController.OpenWindow("endgamewindow");
        _Points = 0;
        failedAnswer = 0;
        remainingTime = 0;
    }

    public void OnAnswer(int id)
    {
        if (currentQuize.answers[id].rightAnswer)
        {
            if (!isStoryGame)
                changeGameScript.OnRightAnswer.Invoke(currentQuize.id, currentCategory.name);
            ButtonAnimations[id].Play("RightAnswer");
            _Points++;
            remainingTime += Time;
        }
        else
        {
            if (doubleTry)
            {
                ButtonAnimations[id].Play("FailAnswer");

            }
            else
            {
                for (int i = 0; i < currentQuize.answers.Count; i++)
                {
                    if (currentQuize.answers[i].rightAnswer)
                    {
                        ButtonAnimations[i].Play("RightAnswer");
                    }
                }
                ButtonAnimations[id].Play("FailAnswer");
                failedAnswer++;
                if (!filedFlag)
                    StartCoroutine(FiledWait());
            }

        }
        if (currentQuize.answers[id].rightAnswer || isStoryGame)
        {
            currentCategory.quizes.Remove(currentQuize);
            GetAnswer = true;
        }
        else
        {
            if (!doubleTry)
                currentCategory.quizes.Remove(currentQuize);
            if (!doubleTry)
                GetAnswer = true;
        }
        Buttons[0].interactable = false;
        Buttons[1].interactable = false;
        Buttons[2].interactable = false;
        Buttons[3].interactable = false;
        doubleTry = false;
    }
    #endregion

    #region CheckStateMethods
    private void StateUpdate()
    {
        TimerProgress.fillAmount = Time / 20f;
        //TimerText.text = Time.ToString();
        //_pointsText.text = string.Format("Счет: {0}", _Points);
    }
    private void CheckAnswer()
    {
        if (GetAnswer)
        {
            Buttons[0].interactable = false;
            Buttons[1].interactable = false;
            Buttons[2].interactable = false;
            Buttons[3].interactable = false;
        }
        else
        {
            Buttons[0].interactable = true;
            Buttons[1].interactable = true;
            Buttons[2].interactable = true;
            Buttons[3].interactable = true;
        }
    }
    private void EnableHint()
    {
        if (answertext != currentQuize.quizeText)
        {
            answertext = currentQuize.quizeText;
            foreach (var item in hints)
            {
                item.interactable = true;
            }
        }
    }
    #endregion
    #region UpdateStateMethods

    /*private void UpdateTimerColor()
    {
        Color green = new Color(0.1877889f, 0.6037736f, 0.08410466f, 1);
        Color red = new Color(0.6037736f, 0.1877889f, 0.08410466f, 1);
        if (Time != time && Time > 5 && Time < 19)
        {
            time = (int)Time;
            currentColor.r += 0.0373231f;
            currentColor.g -= 0.0373231f;
            TimerProgress.color = currentColor;
        }
        if (Time >= 19)
        {
            time = 20;
            TimerProgress.color = green;
            currentColor = green;
        }
    }*/
    #endregion

    private void OnApplicationPause(bool pause)
    {
        //if (pause)
        //Save();
    }
    private void OnApplicationQuit()
    {
        //Save();
    }
    #region QuizEditor
    /*public void WriteText()
    {
        string[] text = new string[Categories.Length];
        text[0] = "Quizes/Litres";
        text[1] = "Quizes/Geography";
        text[2] = "Quizes/History";
        text[3] = "Quizes/Sport";
        text[4] = "Quizes/Tech";
        text[5] = "Quizes/Mythology";
        text[6] = "Quizes/Animals and plants";
        text[7] = "Quizes/Music";
        TextAsset[] textAssets = new TextAsset[Categories.Length];
        for (int i = 0; i < Categories.Length; i++)
        {
            textAssets[i] = Resources.Load<TextAsset>(text[i]);
        }
        for (int i = 0; i < Categories.Length; i++)
        {
            string[] texters = Regex.Split(textAssets[i].text, "\r\n");
            int QuizCount = texters.Length / 5;
            Categories[i].Quizes = new Quize[QuizCount];
            for (int f = 0; f < QuizCount; f++)
            {
                Categories[i].Quizes[f] = new Quize();
                Categories[i].Quizes[f].Answers = new Answer[4];
                for (int n = 0; n < 4; n++)
                {
                    Categories[i].Quizes[f].Answers[n] = new Answer();
                }
            }
            for (int z = 0; z < QuizCount; z++)
            {
                int currentquiz = z * 5;
                Categories[i].Quizes[z].QuizeText = texters[currentquiz];
                Categories[i].Quizes[z].QuizID = z;
                for (int c = 1; c < 5; c++)
                {
                    if (!texters[currentquiz + c].Contains("+"))
                    {
                        Categories[i].Quizes[z].Answers[c - 1].AnswerText = texters[currentquiz + c];
                    }
                    else
                    {
                        string r = texters[currentquiz + c];
                        Categories[i].Quizes[z].Answers[c - 1].AnswerText = r.Replace("+", "");
                        Categories[i].Quizes[z].Answers[c - 1].RightAnswer = true;
                    }

                }
            }
        }
    }
    public void ReadText()
    {
        for (int f = 0; f < Categories.Length; f++)
        {
            string playerDataPath = string.Format("/Quize Please! game/Quiz Please/Assets/Resources/Quizes/{0}.txt", Categories[f].name);
            StreamWriter write = new StreamWriter(playerDataPath);
            string[] vs = new string[Categories[f].Quizes.Length * 5];
            for (int i = 0; i < Categories[f].Quizes.Length; i++)
            {
                int currentquiz = i * 5;
                vs[currentquiz] = Categories[f].Quizes[i].QuizeText;
                for (int z = 1; z < 5; z++)
                {
                    if (Categories[f].Quizes[i].Answers[z - 1].RightAnswer)
                    {
                        vs[currentquiz + z] = Categories[f].Quizes[i].Answers[z - 1].AnswerText + "+";
                    }
                    else
                    {
                        vs[currentquiz + z] = Categories[f].Quizes[i].Answers[z - 1].AnswerText;
                    }
                }
            }
            for (int i = 0; i < vs.Length; i++)
            {
                write.WriteLine(vs[i]);
            }
            write.Flush();
            write.Close();
        }
    }
    public void CheckErrors()
    {
        for (int i = 0; i < Categories.Length; i++)
        {
            bool error = false;
            int s = 0;
            for (int z = 0; z < Categories[i].Quizes.Length; z++)
            {
                if(Categories[i].Quizes[z].QuizID == 0)
                {
                    s++;
                }
                int x = 0;
                for (int c = 0; c < Categories[i].Quizes[z].Answers.Length; c++)
                {
                    if (Categories[i].Quizes[z].Answers[c].RightAnswer)
                    {
                        x++;
                    }
                }
                if(x < 1 || x > 1)
                {
                    Debug.Log(string.Format("В категории {0} вопросе {1} ошибка с ответами(Отметить правильный)", i, z));
                    error = true;
                }
                if(s > 1)
                {
                    Debug.Log(string.Format("В категории {0} ошибка с ID вопросов",i));
                    error = true;
                }
            }
            if (!error)
            {
                Debug.Log(string.Format("В категории {0} ошибок нет", i));
            }
        }
    }*/
    #endregion

    // Update is called once per frame
    void Update()
    {
        //UpdateTimerColor();
        StateUpdate();
        if (startGame == true && iteration != true && paused != true)
        {
            StartCoroutine("Game");
            startGame = false;
        }
        if (Time <= 0 && resourceManager.life > 0 && animated == false)
        {
            if (!filedFlag)
                StartCoroutine(FiledWait());
            failedAnswer++;
            GetAnswer = true;
        }
        if (resourceManager.life <= 0 & iteration == true)
        {
            Debug.Log("Жизни закончились");
            EndGame(1);
            StopCoroutine("Game");
            iteration = false;
        }
        if (paused)
        {
            pausedTime = Time;
            StopCoroutine("Timer");
        }
        if (ButtonAnimations[0].IsPlaying("RightAnswer") | ButtonAnimations[0].IsPlaying("FailAnswer") | ButtonAnimations[1].IsPlaying("RightAnswer") | ButtonAnimations[1].IsPlaying("FailAnswer") | ButtonAnimations[2].IsPlaying("RightAnswer") | ButtonAnimations[2].IsPlaying("FailAnswer") | ButtonAnimations[3].IsPlaying("RightAnswer") | ButtonAnimations[3].IsPlaying("FailAnswer"))
        {
            animated = true;
        }
        else animated = false;
        CheckAnswer();
        if (hints.All(x => !x.interactable))
            EnableHint();
    }
}