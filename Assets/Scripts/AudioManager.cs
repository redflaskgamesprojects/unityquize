﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource MainTheme;
    [SerializeField]
    private AudioSource ClickSound;
    // Start is called before the first frame update
    void Start()
    {
        MainTheme.loop = true;
        MainTheme.Play();
    }
    public void SoundsToggle(bool value)
    {
        ClickSound.enabled = value;
    }
    public void MusicToggle(bool value)
    {
        MainTheme.enabled = value;
    }
    public void PlayClickSound()
    {
        ClickSound.Play();
    }
    // Update is called once per frame
    void Update()
    {
       
    }
}
