﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    private Animation animation;
    private void Start()
    {
        animation = gameObject.GetComponent<Animation>();
    }
    public void Open()
    {
        if (!animation.isPlaying)
            animation.Play("Open");
    }
    public void Close()
    {
        if (!animation.isPlaying)
            animation.Play("Close");
    }
    
}
