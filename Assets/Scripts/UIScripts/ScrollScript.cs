﻿using UnityEngine;

public class ScrollScript : MonoBehaviour
{

    private Vector2 StartPos;
    [SerializeField]
    private Vector2 EndPos;
    private bool isScrolling;
    // Start is called before the first frame update
    void Start()
    {
        StartPos = gameObject.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        float smoothTime = 0.1F;
        float yVelocity = 0.9F;
        if (gameObject.transform.localPosition.y < StartPos.y)
        {
            gameObject.transform.localPosition = new Vector2(0, Mathf.SmoothDamp(gameObject.transform.localPosition.y, StartPos.y, ref yVelocity,Time.deltaTime*10f));
        }
        if (gameObject.transform.localPosition.y > EndPos.y)
        {
            gameObject.transform.localPosition = new Vector2(0, Mathf.SmoothDamp(gameObject.transform.localPosition.y, EndPos.y, ref yVelocity, Time.deltaTime * 10f));
        }
    }
}
