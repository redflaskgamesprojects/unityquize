﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGamePanel : MonoBehaviour
{
    public List<Image> stars;
    public Sprite fullStarSprite;
    public Sprite nullStarSprite;
    public Text experienceText;
    public Text moneyText;
    public Text stateText;
    public Button nextLevelButton;
    public Button adButton;
    public Button exitButton;
    public Button settingsButton;
    public Button menuButton;
}
