﻿using Kilosoft.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static HintsController;

public class HintBehaviour : MonoBehaviour
{
    [SerializeField]
    public HintType hintType;
    [SerializeField]
    HintsController hintsController;
    public Text countText;
    public Button button;
    [EditorButton("Заполнить")]
    public void Test()
    {
        button = GetComponent<Button>();
    }
    private void Start()
    {
        hintsController.AddToList(hintType, this);
    }
    public void OnClickHintButton()
    {
        hintsController.UseHint(hintType);
    }
}
