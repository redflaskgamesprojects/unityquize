﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelPanel : MonoBehaviour
{
    public Button levelButton;
    public Image levelImage;
    public Image lineToNextLevel;
    public Sprite levelPassedSprite;
    public Sprite levelUnpasedSprite;
    public bool passed;
    public void Reset()
    {
        levelButton = GetComponent<Button>();
        levelImage = GetComponent<Image>();
    }
    public void SetLocked()
    {
        if (!passed)
        {
            levelButton.interactable = false;
            levelImage.color = new Color(176 / 255f, 176 / 255f, 176 / 255f);
            if (lineToNextLevel != null)
                lineToNextLevel.color = new Color(176 / 255f, 176 / 255f, 176 / 255f);
        }
    }
    public void SetPassed()
    {
        passed = true;
        levelImage.color = new Color(255 / 255f, 255 / 255f, 255 / 255f);
        levelButton.interactable = false;
        levelImage.sprite = levelPassedSprite;
        lineToNextLevel.color = new Color(99 / 255f, 221 / 255f, 35 / 255f);
    }
    public void SetUpPassed()
    {
        passed = false;
        levelImage.color = new Color(255 / 255f, 255 / 255f, 255 / 255f);
        levelButton.interactable = true;
        levelImage.sprite = levelUnpasedSprite;
        lineToNextLevel.color = new Color(255 / 255f, 255 / 255f, 255 / 255f);
    }
}
