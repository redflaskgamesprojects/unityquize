﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGameController : MonoBehaviour
{
    public ChangeGameScript changeGameScript;
    public Text categoryNameText;
    public string categoryKey;
    public string categoryName;
    public void Init(string categoryName)
    {
        this.categoryName =  categoryName.ToUpper();
        //categoryNameText.text = string.Format("ВЫБРАНА КАТЕГОРИЯ {0}", categoryName).ToUpper();
        categoryKey = categoryName;
    }
    public void StartGame()
    {
        
        changeGameScript.StartGame(categoryKey);
    }
}
