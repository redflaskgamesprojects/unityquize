﻿using UnityEngine;
using System;
using System.Collections.Generic;
[CreateAssetMenu(menuName = "CategoryQuize/Quize",fileName ="New Category")]

[Serializable]
 public class QuizDictionary : SerializableDictionary<int, Quize> { }
[Serializable]
public class Category : ScriptableObject
{
    public int categoryVersion;
    public string nameCategory;
    public QuizDictionary quizes;
}
[Serializable]
public class Quize
{
    public int id;
    public List<Answer> answers;
    public string quizeText;
    public Quize()
    {
        answers = new List<Answer>();
        answers.Add(new Answer());
        answers.Add(new Answer());
        answers.Add(new Answer());
        answers.Add(new Answer());
    }
}
[Serializable]
public class Answer
{
    public string answerText;
    public bool rightAnswer;
}
