﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;

[CreateAssetMenu(menuName = "CategoryQuize/CategoryList", fileName = "New CategoryList")]
[Serializable]
public class AllCategories : ScriptableObject
{
    public LocaleIdentifier locale;
    public List<Category> categories;
}
