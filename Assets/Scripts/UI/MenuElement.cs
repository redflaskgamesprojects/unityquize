﻿using Kilosoft.Tools;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CanvasGroup))]
public class MenuElement : MonoBehaviour
{
    enum TypeElement
    {
        Opened,
        Closed,
        HintsPanel
    }
    [Header("Контроллеры")]
    [SerializeField]
    List<RuntimeAnimatorController> animationControllers;//0 закрытый, 1 открытый, 3 окно подсказок
    [SerializeField]
    TypeElement typeElement;
    [SerializeField]
    Animator animator;
    [SerializeField]
    CanvasGroup canvasGroup;
#if UNITY_EDITOR
    private void Awake()
    {
        SetControllers();
        //ChangeDefault();
    }
    private void Reset()
    {
        SetControllers();
    }
    [EditorButton("Установить контроллеры")]
    public void SetControllers()
    {
        animationControllers = new List<RuntimeAnimatorController>();
        RuntimeAnimatorController animatorController = (RuntimeAnimatorController)EditorGUIUtility.Load("Animations/Controllers/Menus/ClosedMenuController.controller");
        RuntimeAnimatorController animatorController2 = (RuntimeAnimatorController)EditorGUIUtility.Load("Animations/Controllers/Menus/OpenedMenuController.controller");
        RuntimeAnimatorController animatorController3 = (RuntimeAnimatorController)EditorGUIUtility.Load("Animations/Controllers/Menus/HintsPanelController.controller");
        animationControllers.Add(animatorController);
        animationControllers.Add(animatorController2);
        animationControllers.Add(animatorController3);
    }
    [EditorButton("Установить значения")]
    public void ChangeDefault()
    {
        animator = GetComponent<Animator>();
        canvasGroup = GetComponent<CanvasGroup>();
        switch (typeElement)
        {
            case TypeElement.Opened:
                animator.runtimeAnimatorController = animationControllers[1];
                animator.SetBool("active", true);
                canvasGroup.alpha = 1;
                gameObject.SetActive(true);
                break;
            case TypeElement.Closed:
                animator.runtimeAnimatorController = animationControllers[0];
                animator.SetBool("active", false);
                canvasGroup.alpha = 0;
                gameObject.SetActive(false);
                break;
            case TypeElement.HintsPanel:
                animator.runtimeAnimatorController = animationControllers[2];
                animator.SetBool("active", false);
                canvasGroup.alpha = 0;
                gameObject.SetActive(false);
                break;
        }
    }
#endif 
}
