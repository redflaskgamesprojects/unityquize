﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AnimatorDictionary : SerializableDictionary<string, Animator> { }


public class UIController : MonoBehaviour
{
    public AnimatorDictionary animatorDictionary;
    public void CloseWindow(string param)
    {
        StartCoroutine(CloseMenuWait(animatorDictionary[param]));
    }
    IEnumerator CloseMenuWait(Animator animator)
    {
        //yield return new WaitForSeconds(4f);
        animator.SetBool("active", false);
        CanvasGroup canvasGroup = animator.gameObject.GetComponent<CanvasGroup>();
        yield return new WaitWhile(() => canvasGroup.alpha != 0);
        animator.gameObject.SetActive(false);
    }
    public void OpenWindow(string param)
    {
        Animator animator = animatorDictionary[param];
        StartCoroutine(OpenMenuWait(animator));
    }
    IEnumerator OpenMenuWait(Animator animator)
    {
        //yield return new WaitForSeconds(4f);
        animator.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.05f);
        animator.SetBool("active", true);
        yield return null;
    }
    public void ChangeState(string param)
    {
        Animator animator = animatorDictionary[param];
        StartCoroutine(OpenOrCloseWait(animator));
    }
    IEnumerator OpenOrCloseWait(Animator animator)
    {
        //yield return new WaitForSeconds(4f);
        if (animator.gameObject.activeSelf)
        {
            animator.SetBool("active", false);
            CanvasGroup canvasGroup = animator.gameObject.GetComponent<CanvasGroup>();
            yield return new WaitWhile(() => canvasGroup.alpha != 0);
            animator.gameObject.SetActive(false);
        }
        else
        {
            animator.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.05f);
            animator.SetBool("active", true);
            yield return null;
        }
    }
}
