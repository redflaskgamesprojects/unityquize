﻿using Kilosoft.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
public class MenuElementsController : MonoBehaviour
{
    [SerializeField]
    List<MenuElement> menuElements;

    [EditorButton("Найти все контроллеры")]
    public void FindAll()
    {
        menuElements = new List<MenuElement>();
        var s = Resources.FindObjectsOfTypeAll<MenuElement>();
        for (int i = 0; i < s.Length; i++)
        {
            menuElements.Add(s[i]);
        }
    }
    [EditorButton("Установить начальные значения")]
    public void SetAllDefaults()
    {
        foreach (var item in menuElements)
        {
            item.ChangeDefault();
        }
    }
}
#endif