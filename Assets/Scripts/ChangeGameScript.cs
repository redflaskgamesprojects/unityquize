﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.Localization.Tables;

[Serializable]
public class CategoryDataDictionary : SerializableDictionary<int, bool> { }
public class ChangeGameScript : MonoBehaviour
{
    public List<AllCategories> allCategories;
    public CategoryDataSwitcher dataSwitcher;
    public List<Category> categories;
    public GameObject buttonPrefab;
    public List<CategoryButton> categoryButtons;
    public UnityEvent OnQuizeDataChange;
    public RightAnswerEvent OnRightAnswer;
    [SerializeField]
    private GameObject gamePanel;
    [SerializeField]
    private GameObject contentParent;
    public GameObject changeGamePanel;
    private GameScript gameController;
    private ResourceManager resourceController;
    [SerializeField]
    StartGameController startGameController;
    UIController uIController;
    public Locale currentLocale;
    private void OnEnable()
    {
        uIController = GetComponent<UIController>();
        OnQuizeDataChange = new UnityEvent();
        if (OnRightAnswer == null)
            OnRightAnswer = new RightAnswerEvent();
        OnQuizeDataChange.AddListener(CurrentProgress);
        OnRightAnswer.AddListener(RightAnswerHandler);
        LocalizationSettings.SelectedLocaleAsync.Completed += SelectedLocaleAsync_Completed;
        LocalizationSettings.SelectedLocaleChanged += LocalizationSettings_SelectedLocaleChanged;
    }

    private void SelectedLocaleAsync_Completed(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<Locale> obj)
    {
        currentLocale = obj.Result;
        CategoryLocalizeEventHandler();
    }

    private void LocalizationSettings_SelectedLocaleChanged(Locale obj)
    {
        currentLocale = obj;
        CategoryLocalizeEventHandler();
    }

    private void OnDisable()
    {
        OnQuizeDataChange = new UnityEvent();
        if (OnRightAnswer == null)
            OnRightAnswer = new RightAnswerEvent();
        OnQuizeDataChange.RemoveListener(CurrentProgress);
        OnRightAnswer.RemoveListener(RightAnswerHandler);
    }
    IEnumerator Start()
    {
        gameController = GetComponent<GameScript>();
        resourceController = GetComponent<ResourceManager>();
        yield return new WaitWhile(() => currentLocale == null);
        if (!Load())
        {
            SetNullData("ru-RU");
            SetNullData("en");
            NormalizeData();
        }
        SetCategoryButtons();
        OnQuizeDataChange.Invoke();
    }
    public void SetNullData(string idint)
    {
        var categoryDatas = new List<CategoryData>();
        var categoriesTemp = allCategories.Find(x => x.locale == idint).categories;
        for (int i = 0; i < categories.Count; i++)
        {
            categoryDatas.Add(new CategoryData(categoriesTemp[i].nameCategory, categoriesTemp[i].quizes.Values.Count, categoriesTemp[i].categoryVersion));
        }
        switch (idint)
        {
            case "ru-RU":
                dataSwitcher.ruData = categoryDatas;
                break;
            case "en":
                dataSwitcher.engData = categoryDatas;
                break;
        }
    }
    public void SetCategoryButtons()
    {
        if (categoryButtons.Count > 0)
        {
            foreach (var item in categoryButtons)
            {
                Destroy(item.gameObject);
            }
        }
        categoryButtons = new List<CategoryButton>();
        for (int i = 0; i < categories.Count; i++)
        {
            CategoryButton categoryButton = Instantiate(buttonPrefab, contentParent.transform).GetComponent<CategoryButton>();
            categoryButton.controller = startGameController;
            categoryButton.uIController = uIController;
            categoryButtons.Add(categoryButton);
        }
    }
    public void CategoryPassedEvent()
    {

    }
    public void CategoryLocalizeEventHandler()
    {
        try
        {
            categories = allCategories.Find(x => x.locale == currentLocale.Identifier).categories;
            SetCategoryButtons();
            OnQuizeDataChange.Invoke();
            Debug.Log("Сменил");
        }
        catch
        {
            Debug.Log("Нет локализированных данных");
        }
    }
    public void StartGame(string categoryName)
    {
        uIController.CloseWindow("startgamepanel");
        uIController.CloseWindow("choicecategorymenu");
        uIController.CloseWindow("menuwindows");
        PlayingCategory playingCategory = new PlayingCategory(categoryName);
        Category pickedCategory = categories.Find(x => x.nameCategory == categoryName);
        Debug.Log(dataSwitcher.GetData(currentLocale));
        List<CategoryData> temp = dataSwitcher.GetData(currentLocale);
        CategoryData currentData = temp.Find(x => x.nameCategory == categoryName);
        if (currentData.passedQuizes != null)
        {
            CategoryDataDictionary list = currentData.passedQuizes;
            int index = 0;
            foreach (var item in list)
            {
                if (!item.Value)
                    playingCategory.quizes.Add(pickedCategory.quizes[item.Key]);
                index++;
            }
        }
        else
        {
            List<Quize> quizes = pickedCategory.quizes.Values.ToList();
            foreach (var item in quizes)
            {
                playingCategory.quizes.Add(item);
            }

        }
        gameController.currentCategory = playingCategory;
        uIController.OpenWindow("gamepanel");
        gamePanel.SetActive(true);
        gameController.StartGame();
    }
    #region ProgressMethods
    public void RightAnswerHandler(int id, string name)
    {
        foreach (var item in dataSwitcher.GetData(currentLocale))
        {
            if (item.nameCategory == name)
            {
                item.passedQuizes[id] = true;
            }
        }
        CurrentProgress();
    }
    private void UpdatePassedQuestion()
    {
        if(currentLocale != null)
        {
            for (int i = 0; i < categoryButtons.Count; i++)
            {
                if (dataSwitcher.GetData(currentLocale)[i].passedQuizes.AsDictionary.Values.All(x => x) || resourceController.life <= 0)
                {
                    categoryButtons[i].button.interactable = false;
                }
                else
                {
                    categoryButtons[i].button.interactable = true;
                }
            }
        }
    }
    public void CurrentProgress()
    {
        int currentIndex = 0;
        List<CategoryData> temp = dataSwitcher.GetData(currentLocale);
        foreach (var item in temp)
        {
            int countPassed = 0;
            foreach (var items in item.passedQuizes.AsDictionary.Values)
            {
                if (items)
                {
                    countPassed++;
                }
            }
            categoryButtons[currentIndex].categoryName.text = categories[currentIndex].nameCategory;
            //categoryButtons[currentIndex].level.text = string.Format("{0}ур", (int)Math.Truncate(countPassed / 10d) + 1);
            categoryButtons[currentIndex].passedQuizes.text = string.Format("{0}/{1}", countPassed, item.passedQuizes.AsDictionary.Count);
            currentIndex++;
        }
        UpdatePassedQuestion();
    }
    #endregion
    private void OnApplicationQuit()
    {
        Save();
    }
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            Save();
        }
    }
    #region SavedData
    public void Save()
    {
        string jsonRu = JsonConvert.SerializeObject(dataSwitcher.ruData);
        string jsonEng = JsonConvert.SerializeObject(dataSwitcher.engData);
        PlayerPrefs.SetString("QuizSave", jsonRu);
        PlayerPrefs.SetString("QuizSave2", jsonEng);
    }
    public void NormalizeData()
    {
        if(currentLocale.Identifier.Code == "ru-RU")
        {
            foreach (var item in dataSwitcher.ruData)
            {
                Category currentCheck = categories.Find(x => x.nameCategory == item.nameCategory);
                foreach (var saveKeys in item.passedQuizes.Keys)
                {
                    if (!currentCheck.quizes.AsDictionary.Keys.Contains(saveKeys))
                    {
                        item.passedQuizes.Remove(saveKeys);
                    }
                }
                foreach (var quizeKey in currentCheck.quizes.Keys)
                {
                    if (!item.passedQuizes.AsDictionary.Keys.Contains(quizeKey))
                    {
                        item.passedQuizes.Add(quizeKey, false);
                    }
                }
            }
        }
        if (currentLocale.Identifier.Code == "en")
        {
            foreach (var item in dataSwitcher.engData)
            {
                Category currentCheck = categories.Find(x => x.nameCategory == item.nameCategory);
                foreach (var saveKeys in item.passedQuizes.Keys)
                {
                    if (!currentCheck.quizes.AsDictionary.Keys.Contains(saveKeys))
                    {
                        item.passedQuizes.Remove(saveKeys);
                    }
                }
                foreach (var quizeKey in currentCheck.quizes.Keys)
                {
                    if (!item.passedQuizes.AsDictionary.Keys.Contains(quizeKey))
                    {
                        item.passedQuizes.Add(quizeKey, false);
                    }
                }
            }
        }
    }
    public bool Load()
    {
        bool ru = false;
        bool eng = false;

        if (PlayerPrefs.HasKey("QuizSave"))
        {
            string json = PlayerPrefs.GetString("QuizSave");
            List<CategoryData> saveData = new List<CategoryData>();
            var categoryDatasTemp = JsonConvert.DeserializeObject<List<CategoryData>>(json);
            int c = 0;
            foreach (var item in categoryDatasTemp)
            {
                Category currentCheck = allCategories.Find(x => x.locale == "ru-RU").categories.Find(x => x.nameCategory == item.nameCategory);
                foreach (var saveKeys in item.passedQuizes.Keys)
                {
                    if (!currentCheck.quizes.AsDictionary.Keys.Contains(saveKeys))
                    {
                        item.passedQuizes.Remove(saveKeys);
                    }

                }
                foreach (var quizeKey in currentCheck.quizes.Keys)
                {
                    if (!item.passedQuizes.AsDictionary.Keys.Contains(quizeKey))
                    {
                        item.passedQuizes.Add(quizeKey, false);
                    }
                }
                int countPassed = 0;
                foreach (var itemPassed in item.passedQuizes.Values)
                {
                    if (itemPassed)
                        countPassed++;
                }
                Debug.Log(string.Format("Загруженно {0} пройденых вопросов в категории", countPassed));
            }
            dataSwitcher.ruData = categoryDatasTemp;
            ru = true;
        }
        else
        {
            Debug.Log("Нет загруженных ru вопросов");
            ru = false;
        }
        if (PlayerPrefs.HasKey("QuizSave2"))
        {
            string json = PlayerPrefs.GetString("QuizSave2");
            List<CategoryData> saveData = new List<CategoryData>();
            var categoryDatasTemp = JsonConvert.DeserializeObject<List<CategoryData>>(json);
            int c = 0;
            foreach (var item in categoryDatasTemp)
            {
                Category currentCheck = allCategories.Find(x => x.locale == "en").categories.Find(x => x.nameCategory == item.nameCategory);
                foreach (var saveKeys in item.passedQuizes.Keys)
                {
                    if (!currentCheck.quizes.AsDictionary.Keys.Contains(saveKeys))
                    {
                        item.passedQuizes.Remove(saveKeys);
                    }

                }
                foreach (var quizeKey in currentCheck.quizes.Keys)
                {
                    if (!item.passedQuizes.AsDictionary.Keys.Contains(quizeKey))
                    {
                        item.passedQuizes.Add(quizeKey, false);
                    }
                }
                int countPassed = 0;
                foreach (var itemPassed in item.passedQuizes.Values)
                {
                    if (itemPassed)
                        countPassed++;
                }
                Debug.Log(string.Format("Загруженно {0} пройденых вопросов в категории", countPassed));
            }
            dataSwitcher.engData = categoryDatasTemp;
            eng = true;
        }
        else
        {
            Debug.Log("Нет загруженных eng вопросов");
            eng = false;
        }
        if (eng || ru)
        {
            return true;
        }
        else
            return false;
    }
    #endregion

    // Update is called once per frame
    void FixedUpdate()
    {
       // UpdatePassedQuestion();
    }
}
public class RightAnswerEvent : UnityEvent<int, string> { }
[Serializable]
public class PlayingCategory
{
    public string name;
    public List<Quize> quizes;
    public PlayingCategory(string name)
    {
        this.name = name;
        quizes = new List<Quize>();
    }
}
[Serializable]
public class CategoryData
{
    public int savesVersion;
    public string nameCategory;
    public CategoryDataDictionary passedQuizes;
    public CategoryData(string name, int count, int savesVersion)
    {
        this.savesVersion = savesVersion;
        nameCategory = name;
        passedQuizes = new CategoryDataDictionary();
        for (int i = 0; i < count; i++)
        {
            passedQuizes.Add(i, false);
        }
    }
}
public class EventsSubmitted
{
    public List<bool> eventsSubmited;

}
[Serializable]
public class CategoryDataSwitcher
{
    public List<CategoryData> ruData;
    public List<CategoryData> engData;
    public string ruCode = "ru-RU";
    public string engCode = "en";
    public List<CategoryData> GetData(Locale locale)
    {
        List<CategoryData> temp = new List<CategoryData>();
        if (locale.Identifier.Code == ruCode)
        {
            temp = ruData;
        }
        if(locale.Identifier.Code == engCode)
        {
            temp = engData;
        }
        return temp;
    }
}