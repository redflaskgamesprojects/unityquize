﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CategoriesInspector))]
public class EditorCategories : Editor
{
    public SerializedProperty property;
    public MyWindow myWindow;
    public string name = "";
    bool foldStatistic;
    bool foldOldVersions;
    private void OnEnable()
    {
        myWindow = new MyWindow();
    }
    public override void OnInspectorGUI()
    {
        property = serializedObject.FindProperty("category");
        EditorGUILayout.PropertyField(property);
        if (property.objectReferenceValue != null)
        {
            Category category = property.objectReferenceValue as Category;
            category.nameCategory = EditorGUILayout.TextField("Название категории", category.nameCategory);
            if (category.quizes != null && category.quizes.Count > 0)
                SetButtons(category);
            if (GUILayout.Button("Добавить вопрос"))
            {
                Quize newQuiz = new Quize();
                if(category.quizes.Count > 0)
                newQuiz.id = category.quizes.Keys.Last()+1;
                else
                    newQuiz.id=0;
                category.quizes.Add(newQuiz.id,newQuiz);
                myWindow.Init(newQuiz, category);
            }
            if (GUILayout.Button("Быстрое наполнение"))
            {
                ResourcesLoadQuizes(category);
            }
            GUILayout.BeginVertical("Статистика", "window");
            EditorGUILayout.SelectableLabel(string.Format("Количество вопросов: {0}", category.quizes.Count.ToString()));
            foldStatistic = EditorGUILayout.Foldout(foldStatistic, "Проверка на ошибки");
            if (foldStatistic)
            {
                GUILayout.BeginVertical("Проверка", "window");
                foreach (var item in category.quizes)
                {
                    int rightAnswersCount = 0;
                    for (int i = 0; i < item.Value.answers.Count; i++)
                    {
                        if (item.Value.answers[i].rightAnswer)
                            rightAnswersCount++;
                    }
                    if (rightAnswersCount > 1)
                        EditorGUILayout.SelectableLabel(string.Format("Вопрос {0} c двумя правильными ответами", item.Value.id.ToString()));
                }
                foreach (var item in category.quizes)
                {
                    if (item.Value.answers.All(x => !x.rightAnswer))
                        EditorGUILayout.SelectableLabel(string.Format("Вопрос {0} без правильного ответа", item.Value.id.ToString()));
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndVertical();
#if UNITY_EDITOR
            property.serializedObject.ApplyModifiedProperties();

            EditorUtility.SetDirty(category);
#endif
        }

    }
    public void SetButtons(Category category)
    {
        foreach (var item in category.quizes)
        {
            NamedGUILayout.Button(item.Value.id.ToString());
        }
        string clickedName;
        if (NamedGUILayout.TryGetNameOfJustClickedButton(out clickedName))
        {
            int id = int.Parse(clickedName);
            myWindow.Init(category.quizes[id], category);
        }
    }
    public void ResourcesLoadQuizes(Category category)
    {
        TextAsset textAssets = Resources.Load<TextAsset>(string.Format("Quizes/{0}", category.nameCategory));
        string[] texters = Regex.Split(textAssets.text, "\r\n");
        int QuizCount = texters.Length / 5;
        category.quizes = new QuizDictionary();
        for (int f = 0; f < QuizCount; f++)
        {
            category.quizes.Add(f,new Quize());
        }
        for (int z = 0; z < QuizCount; z++)
        {
            int currentquiz = z * 5;
            category.quizes[z].quizeText = texters[currentquiz];
            category.quizes[z].id = z;
            for (int c = 1; c < 5; c++)
            {
                if (!texters[currentquiz + c].Contains("+"))
                {
                    category.quizes[z].answers[c - 1].answerText = texters[currentquiz + c];
                }
                else
                {
                    string r = texters[currentquiz + c];
                    category.quizes[z].answers[c - 1].answerText = r.Replace("+", "");
                    category.quizes[z].answers[c - 1].rightAnswer = true;
                }
            }
        }
    }
}
public class MyWindow : EditorWindow
{
    bool groupEnabled;
    public Quize quize;
    public Category category;
    public MyWindow currentWindow;
    public void Init(Quize quize, Category category)
    {

        MyWindow window = (MyWindow)EditorWindow.GetWindow(typeof(MyWindow));
        if (window == null)
        {
            this.category = category;
            this.quize = quize;
            title = string.Format("Редактор вопроса {0}", this.quize.id);
            currentWindow = window;
        }
        else
        {
            window.quize = quize;
            window.category = category;
            title = string.Format("Редактор вопроса {0}", window.quize.id);
            window.currentWindow = window;
        }
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.BeginVertical("Вопрос", "window");
        quize.quizeText = EditorGUILayout.TextField("Текст вопроса", quize.quizeText);
        EditorGUILayout.Space(5);
        foreach (var item in quize.answers)
        {
            EditorGUILayout.Space(2);
            item.answerText = EditorGUILayout.TextField("Текст ответа", item.answerText);
            item.rightAnswer = EditorGUILayout.Toggle("Правильный ответ?", item.rightAnswer);
        }
        GUILayout.EndVertical();
        if (GUILayout.Button("Удалить сообщение"))
            DeleteMessage();
    }
    public void DeleteMessage()
    {
        currentWindow = (MyWindow)GetWindow(typeof(MyWindow));
        int id = quize.id;
        category.quizes.Remove(quize.id);
        category.quizes.Distinct();
        //category.quizes.Values.Remove(quize);
        currentWindow.Close();
    }
}
public static class NamedGUILayout
{
    public static bool ButtonWasJustClicked
    {
        get;
        private set;
    }

    public static string LastClickedButtonName
    {
        get;
        private set;
    }

    public static bool Button(string label)
    {
        return Button(new GUIContent(label), label);
    }

    public static bool Button(GUIContent label, string controlName)
    {
        if (ButtonWasJustClicked)
        {
            if (string.Equals(LastClickedButtonName, controlName, StringComparison.Ordinal))
            {
                ButtonWasJustClicked = false;
            }
        }

        if (GUILayout.Button(label))
        {
            ButtonWasJustClicked = true;
            LastClickedButtonName = controlName;
            return true;
        }
        return false;
    }

    public static bool TryGetNameOfJustClickedButton(out string name)
    {
        if (ButtonWasJustClicked)
        {
            name = LastClickedButtonName;
            return true;
        }
        name = "";
        return false;
    }
}
