﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Kilosoft.Tools;
using Newtonsoft.Json;
using UnityEngine.UI;

public class DailyRewardsController : MonoBehaviour
{
    [SerializeReference]
    List<IReward> rewards;
    [SerializeField]
    List<RewardPanel> rewardPanels;
    [SerializeField]
    Button getRewardButton;
    [SerializeField]
    public GameObject rewardAvailable;
    public int currentDay;
    public bool rewardReceived;
    ResourceManager resourceManager;
    public int currentTime;
    void Start()
    {
        resourceManager = GetComponent<ResourceManager>();
        LoadData();

    }
    [EditorButton("Инициализировать награды")]
    public void Init()
    {
        resourceManager = GetComponent<ResourceManager>();
        rewards = new List<IReward>();
        rewards.Add(new RewardMoney(50));
        rewards.Add(new RewardLife(2));
        rewards.Add(new RewardMoney(75));
        rewards.Add(new RewardMoney(100));
        rewards.Add(new RewardLife(4));
        rewards.Add(new RewardMoney(100));
        rewards.Add(new RewardLastDay(100));
    }
    public void SetDay(int addedTime)
    {
        currentTime += addedTime;
        int lastDay = currentDay;
        if(currentTime >= 86400)
        {
            currentDay++;
            currentDay = currentDay % rewards.Count;
            currentTime -= currentTime;
        }
        if (!rewardReceived)
        {
            currentDay = 0;
        }
        if(lastDay != currentDay)
        {
            rewardReceived = false;
        }
        UISet();
    }
    public void UISet()
    {
        int count = 0;
        foreach (var item in rewardPanels)
        {
            if (!rewardReceived)
            {
                if (currentDay != count)
                {
                    item.canvasGroup.alpha = 0.4f;
                    item.warningImage.SetActive(false);
                }
                else
                {
                    item.canvasGroup.alpha = 1f;
                    item.warningImage.SetActive(true);
                    if (!getRewardButton.interactable)
                    {
                        getRewardButton.interactable = true;
                        getRewardButton.gameObject.GetComponent<CanvasGroup>().alpha = 1f;
                    }
                    rewardAvailable.SetActive(true);
                }
            }
            else
            {
                item.canvasGroup.alpha = 0.4f;
                item.warningImage.SetActive(false);
                if (getRewardButton.interactable)
                {
                    getRewardButton.interactable = false;
                    getRewardButton.gameObject.GetComponent<CanvasGroup>().alpha = 0.5f;
                    rewardAvailable.SetActive(false);
                }
            }
            count++;
        }  
    }
    private void OnApplicationQuit()
    {
        SaveData();
    }
    private void OnApplicationPause(bool pause)
    {
        if (pause)
            SaveData();
    }
    public void GetReward()
    {
        rewards[currentDay % rewards.Count].AddReward(resourceManager);
        rewardReceived = true;
        Firebase.Analytics.FirebaseAnalytics.LogEvent(string.Format("reward_{0}_received", currentDay));
        UISet();
    }
    public void SaveData()
    {
        RewardsData data = new RewardsData(this);
        string json = JsonConvert.SerializeObject(data);
        Debug.Log(json);
        PlayerPrefs.SetString("DailyRewards", json);
    }
    public void LoadData()
    {
        if (PlayerPrefs.HasKey("DailyRewards"))
        {
            string json = PlayerPrefs.GetString("DailyRewards");
            RewardsData data = JsonConvert.DeserializeObject<RewardsData>(json);
            Debug.Log(string.Format("День:{0} Получена?:{1}",data.currentDay,data.rewardReceived));
            data.SetData(this);
        }
        UISet();
    }
}
public class RewardsData
{
    public int currentDay;
    public bool rewardReceived;
    public int currentTime;
    public RewardsData()
    {

    }
    public RewardsData(DailyRewardsController rewardsController)
    {
        this.currentTime = rewardsController.currentTime;
        currentDay = rewardsController.currentDay;
        rewardReceived = rewardsController.rewardReceived;
    }
    public void SetData(DailyRewardsController rewardsController)
    {
        rewardsController.currentTime = this.currentTime;
        rewardsController.currentDay = currentDay;
        rewardsController.rewardReceived = rewardReceived;
    }
}
#region RewardsClasses
public interface IReward
{
    void AddReward(ResourceManager resourceManager);
}
[Serializable]
public class RewardMoney : IReward
{
    public int count;
   public void AddReward(ResourceManager resourceManager)
   {
        Debug.Log("Добавляю " + count + " денег");
        resourceManager.money += count;
        Debug.Log("Добавил " + count + " денег");
    }
    public RewardMoney(int count)
    {
        this.count = count;
    }
}
[Serializable]
public class RewardLife : IReward
{
    public int count;
    public void AddReward(ResourceManager resourceManager)
    {
        resourceManager.life += count;
    }
    public RewardLife(int count)
    {
        this.count = count;
    }
}
[Serializable]
public class RewardLastDay : IReward
{
    public int countMoney;
    public void AddReward(ResourceManager resourceManager)
    {
        resourceManager.money += countMoney;
        resourceManager.tipChanceCount++;
        resourceManager.tipSkipCount++;
        resourceManager.tipDoubleCount++;
        resourceManager.hintsCountChanged.Invoke();
    }
    public RewardLastDay(int countMoney)
    {
       this.countMoney = countMoney;
    }
}
#endregion