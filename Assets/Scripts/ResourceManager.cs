﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
[Serializable]
public class ResourceManager : MonoBehaviour
{
    [SerializeField]
    private Text _MoneyText, _LifeText, _RecoveryTimeText;
    public int money;
    public int life, _maxLife = 10;
    public int tipDoubleCount = 1;
    public int tipChanceCount = 1;
    public int tipSkipCount = 1;
    public int tipAddTimeCount =1;
    [SerializeField]
    private int time, _maxTime = 600;
    private bool CouroutineWork;
    public UnityEvent hintsCountChanged;
    // Start is called before the first frame update
    private void Awake()
    {
        hintsCountChanged = new UnityEvent();
    }
    void Start()
    {
        time = _maxTime;
        LoadData();
    }
    #region RecoveryMethods
    IEnumerator LifeTimer()//Корутин который запускает отсчет 
    {
        CouroutineWork = true;
        while (time != 0)
        {
            yield return new WaitForSeconds(1);
            time--;
        }
        life++;
        time = _maxTime;
        CouroutineWork = false;
    }
    public void TimeLeftLife(int time)
    {
        double lifeAddCount = (double) time / _maxTime;
        int lifeInteger =(int)Math.Truncate(lifeAddCount);
        double timeRemnant = lifeAddCount - lifeInteger;

        int AddCount = Mathf.Clamp(lifeInteger, 0, _maxLife - life);
        life += AddCount;
        if (life < _maxLife)
        {
            double timeLast = timeRemnant * _maxTime;
            this.time -= Mathf.Clamp((int)Math.Truncate(timeLast),0,time);
        } 
    }
    
    public void Timer()//Запускает корутин, если учтены условия
    {
        if (life < _maxLife)
        {
            if (!CouroutineWork)
            {
                StartCoroutine("LifeTimer");
            }
        }
        else
        {
            time = _maxTime;
            StopCoroutine("EnergyTimer");
        }
    }
    #endregion
    #region UpdateStatesMethods
    private void TextUpdate()
    {
        var ts = TimeSpan.FromSeconds(this.time);
        string time = string.Format("{1}:{2}", ts.Hours, ts.Minutes, ts.Seconds);
        if (ts.Minutes < 10)
        {
            time = string.Format("0{1}:{2}", ts.Hours, ts.Minutes, ts.Seconds);
            if (ts.Seconds < 10)
            {
                time = string.Format("0{1}:0{2}", ts.Hours, ts.Minutes, ts.Seconds);
            }
        }
        if (ts.Seconds < 10 & ts.Minutes > 10)
        {
            time = string.Format("{1}:0{2}", ts.Hours, ts.Minutes, ts.Seconds);
        }
        if (life >= _maxLife) time = "";
        _RecoveryTimeText.text = time;
       // _RecoveryTimeText2.text = time;
        _LifeText.text = string.Format("{0}/10", life);
        //_LifeText2.text = string.Format("{0}/10", _life);
        _MoneyText.text = string.Format("{0}", money);
    }
    #endregion
    #region SaveDataMethods
    private void SaveData()
    {
        Data resource = new Data();
        resource.life = life;
        resource.money = money;
        resource.time = time;
        resource.tipAddTimeCount = tipAddTimeCount;
        resource.tipChanceCount = tipChanceCount;
        resource.tipDoubleCount = tipDoubleCount;
        resource.tipSkipCount = tipSkipCount;
        string saveData = JsonUtility.ToJson(resource);
        PlayerPrefs.SetString("Resource", saveData);
        Debug.Log("Данные сохранены" + PlayerPrefs.GetString("Resource"));
    }
    private void LoadData()
    {
        if (PlayerPrefs.HasKey("Resource"))
        {
            string loadData = PlayerPrefs.GetString("Resource");
            Data data = JsonUtility.FromJson<Data>(loadData);
            Debug.Log(string.Format("Загруженные значения:  Жизнь:{0}  Деньги:{1}  Время:{2}", data.life, data.money, data.time));
            life = data.life;
            money = data.money;
            time = data.time;
            tipAddTimeCount = data.tipAddTimeCount;
            tipChanceCount = data.tipChanceCount;
            tipDoubleCount = data.tipDoubleCount;
            tipSkipCount = data.tipSkipCount;
            //Debug.Log("Данные загружены" + loadData);
        }
    }
    #endregion
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            SaveData();
        }
    }
    private void OnApplicationQuit()
    {
        SaveData();
    }
    private void FixedUpdate()
    {
        TextUpdate();
    }
    // Update is called once per frame
    void Update()
    {
        Timer();
    }
}
[Serializable]
public class Data
{
    public int money;
    public int life;
    public int time;
    public int tipDoubleCount;
    public int tipChanceCount;
    public int tipSkipCount;
    public int tipAddTimeCount;
}
