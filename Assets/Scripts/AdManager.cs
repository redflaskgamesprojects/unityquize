﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;


public class AdManager : MonoBehaviour
{
    [SerializeField]
    private GameObject RewardPanel;
    private ResourceManager script;
    private const string _adInterstitialKey = "ca-app-pub-1020427055934722/4404874900";
    private const string _rewardedAdKey = "ca-app-pub-1020427055934722/6130796212";
    /// <summary>
    /// //
    /// </summary>
    private const string _TestrewardedAdKey = "	ca-app-pub-3940256099942544/5224354917";
    private const string _TestedPageKey = "ca-app-pub-3940256099942544/1033173712";
    private InterstitialAd _adPage;
    public RewardedAd _RewardedLife;
    private AdRequest request;
    private const string appId = "ca-app-pub-1020427055934722~6132696025";
    private void Start()
    {
        MobileAds.Initialize(appId);
        script = GetComponent<ResourceManager>();
    }
    #region LoadMethods
    public void ShowAdPage()
    {
        _adPage = new InterstitialAd(_adInterstitialKey);
        request = new AdRequest.Builder().Build();
        _adPage.LoadAd(request);
        _adPage.OnAdLoaded += OnAdLoaded;
        _adPage.OnAdFailedToLoad += _adPage_OnAdFailedToLoad;
    }
    public void ShowRewarded()
    {
        request = new AdRequest.Builder().Build();
        _RewardedLife = new RewardedAd(_rewardedAdKey);
        _RewardedLife.LoadAd(request);
        _RewardedLife.OnAdLoaded += _RewardedLife_OnAdLoaded;
        _RewardedLife.OnAdFailedToLoad += _RewardedLife_OnAdFailedToLoad;
    }
    #endregion
    #region HandleMethdos
    //Interstitial
    public void OnAdLoaded(object sender, System.EventArgs args)
    {
        _adPage.Show();
    }
    private void _adPage_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {
        ShowAdPage();
    }

    //Rewarded
    private void _RewardedLife_OnAdLoaded(object sender, System.EventArgs e)
    {
        _RewardedLife.Show();
        _RewardedLife.OnUserEarnedReward += _RewardedLife_OnUserEarnedReward;
    }

    private void _RewardedLife_OnUserEarnedReward(object sender, Reward e)
    {
        script.money += (int)e.Amount;
    }
    private void _RewardedLife_OnAdFailedToLoad(object sender, AdErrorEventArgs e)
    {
        ShowRewarded();
    }

    #endregion
    void Update()
    {
    }
}
