﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class LevelsDictionary : SerializableDictionary<string, Category> { }
public class OnLevelEndEvent : UnityEvent<string> { }
public class StoryModeController : MonoBehaviour
{
    [SerializeField]
    List<Category> levels;
    public List<LevelData> levelsData;
    public UnityEvent OnLevelDataChange;
    public List<LevelPanel> levelPanels;
    private UIController uIController;
    private GameScript gameController;
    public OnLevelEndEvent OnLevelEnd;
    ResourceManager resourceManager;
    public string currentLevel;
    public string maxLevel;
    public void Start()
    {
        uIController = GetComponent<UIController>();
        gameController = GetComponent<GameScript>();
        OnLevelDataChange = new UnityEvent();
        OnLevelEnd = new OnLevelEndEvent();
        resourceManager = GetComponent<ResourceManager>();
        if (!Load())
        {
            levelsData = new List<LevelData>();
            foreach (var item in levels)
            {
                levelsData.Add(new LevelData(item.nameCategory));
            }
        }
        OnLevelDataChange.AddListener(SetUI);
        OnLevelEnd.AddListener(LevelPassed);
        OnLevelDataChange.Invoke();
    }
    public void StartGame(string levelName)
    {
        currentLevel = levelName.Replace("level","");
        uIController.CloseWindow("storymodemenu");
        uIController.CloseWindow("menuwindows");
        PlayingCategory playingCategory = new PlayingCategory(levelName);
        Category pickedCategory = levels.Find(x => x.nameCategory == levelName);
        foreach (var item in pickedCategory.quizes)
        {
            playingCategory.quizes.Add(item.Value);
        }

        gameController.currentCategory = playingCategory;
        gameController.isStoryGame = true;
        uIController.OpenWindow("gamepanel");
        gameController.StartGame();
    }
    public void StartNext()
    {
        uIController.CloseWindow("endgamewindow");
        int level = int.Parse(currentLevel);
        level++;
        StartGame("level" + level);
    }
    public void LevelPassed(string levelName)
    {
        levelsData.Find(x => x.nameCategory == levelName).passed = true;
        Firebase.Analytics.FirebaseAnalytics.LogEvent(string.Format("{0}_passed", levelName));
        OnLevelDataChange.Invoke();
    }
    public void SetUI()
    {
        int i = 0;
        bool firstUnPassed = false;
        foreach (var item in levelPanels)
        {
            if (levelsData[i].passed)
            {
                item.SetPassed();
            }
            else if(!levelsData[i].passed && !firstUnPassed)
            {
                firstUnPassed = true;
                item.SetUpPassed();
                if (resourceManager.life < 1)
                {
                    item.SetLocked();
                }
                continue;
            }
            if(firstUnPassed)
            {
                item.SetLocked();
            }
            
            i++;
        }
    }
    public void FixedUpdate()
    {
        SetUI();
    }
    #region SavedData
    private void OnApplicationQuit()
    {
        Save();
    }
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            Save();
        }
    }
    public void Save()
    {
        string key = "LevelsData";
        string json = JsonConvert.SerializeObject(levelsData);
        PlayerPrefs.SetString(key, json);
        Debug.Log("Сохранил данные уровней: " + json);
    }
    public bool Load()
    {
        string key = "LevelsData";
        if (PlayerPrefs.HasKey(key))
        {
            string json = PlayerPrefs.GetString(key);
            List<CategoryData> saveData = new List<CategoryData>();
            levelsData = JsonConvert.DeserializeObject<List<LevelData>>(json);
            int count = 0;
            foreach (var item in levelsData)
            {
                if (item.passed)
                    count++;
            }
            Debug.Log(string.Format("Загруженно {0} пройденых уровней", count));
            return true;
        }
        else
        {
            Debug.Log("Нет загруженных уровней");
            return false;
        }
    }

    #endregion
}

[Serializable]
public class LevelData
{
    public string nameCategory;
    public bool passed;
    public LevelData(string name)
    {
        nameCategory = name;
    }
}