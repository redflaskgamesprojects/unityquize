﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TimeSystem : MonoBehaviour
{
    private int timeLeft;
    private int timeLastSession;
    [SerializeField][HideInInspector]
    private TimeData timeNow;
    private int SessionTime;
    private ResourceManager script;
    public bool isTrueConnected;
    DailyRewardsController dailyRewardsController;
    public List<string> list;
    int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        dailyRewardsController = GetComponent<DailyRewardsController>();
        script = GetComponent<ResourceManager>();
        //StartCoroutine(CheckOnline());
        timeNow = new TimeData();
        list = new List<string>();
        list.Add("http://f0499496.xsph.ru/time");
        list.Add("https://showcase.api.linx.twenty57.net/UnixTime/tounixtimestamp?datetime=now");
    }
    public IEnumerator CheckOnline()
    {
        bool isConnected = false;
        UnityWebRequest unityWebRequest = null;
        while (!isConnected)
        {
            yield return new WaitForSeconds(2f);
            unityWebRequest = UnityWebRequest.Get(list[index % list.Count]);
            yield return unityWebRequest.SendWebRequest();
            if (unityWebRequest.isNetworkError)
            {
                Debug.Log("Error: " + unityWebRequest.error);
                index++;
                Firebase.Analytics.FirebaseAnalytics.LogEvent(string.Format("network_error{0}",index));
            }
            else
            {
                isConnected = true;
            }
        }
        string timeString = unityWebRequest.downloadHandler.text;
        timeNow = JsonConvert.DeserializeObject<TimeData>(timeString);
        if (PlayerPrefs.HasKey("LastSession"))
        {
            timeLastSession = int.Parse(PlayerPrefs.GetString("LastSession"));
            timeLeft = timeNow.UnixTimeStamp - timeLastSession;
        }
        else
        {
            timeLeft = 0;
        }
        Debug.Log("Интернет проверка!");
        StartCoroutine(SessionTimer());
        dailyRewardsController.SetDay(timeLeft);
        script.TimeLeftLife(timeLeft);
        //Вычитаем из таймера
        isTrueConnected = true;
        yield return null;
    }
    IEnumerator SessionTimer()
    {
        while (true)
        {
            SessionTime++;
            yield return new WaitForSecondsRealtime(1);
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            PlayerPrefs.SetString("LastSession", (timeNow.UnixTimeStamp + SessionTime).ToString());
            Debug.Log("Время сохранено:" + PlayerPrefs.GetString("LastSession"));
        }

    }
    private void OnApplicationQuit()
    {
        PlayerPrefs.SetString("LastSession", (timeNow.UnixTimeStamp + SessionTime).ToString());
        Debug.Log("Время сохранено:" + PlayerPrefs.GetString("LastSession"));
    }
}
[Serializable]
public class TimeData
{
    public int UnixTimeStamp;
}
