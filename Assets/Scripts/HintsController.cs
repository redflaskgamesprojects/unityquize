﻿using Kilosoft.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static HintsController;

public class HintsController : MonoBehaviour
{
    [Serializable]
    public enum HintType
    {
        ChanceHint,
        SkipQuestHint,
        AddTimeHint,
        DoubleTryHint
    }
    public Dictionary<HintType,IHint> hints;
    private GameScript gameScript;
    private ResourceManager resourceManager;
    #region UIVariables
    public Dictionary<HintType,HintBehaviour> hintsButtons;

    #endregion
    public void Awake()
    {
        gameScript = GetComponent<GameScript>();
        resourceManager = GetComponent<ResourceManager>();
    }
    IEnumerator Start()
    {
        Init();
        resourceManager.hintsCountChanged.AddListener(SetUI);
        hintsButtons = new Dictionary<HintType, HintBehaviour>();
        yield return new WaitWhile(() => hintsButtons.Count < 4);
        SetUI();
    }
    [EditorButton("Обнавить UI")]
    public void Test()
    {
        resourceManager.hintsCountChanged.Invoke();
    }
    public void Init()
    {
        hints = new Dictionary<HintType, IHint>();
        hints.Add(HintType.DoubleTryHint, new DoubleTryHint());
        hints.Add(HintType.ChanceHint, new ChanceHint());
        hints.Add(HintType.SkipQuestHint, new SkipQuestHint());
        hints.Add(HintType.AddTimeHint, new AddTimeHint());
        
    }
    public void AddToList(HintType hintType,HintBehaviour hintBehaviour)
    {
        hintsButtons.Add(hintType,hintBehaviour);
    }
    public void UseHint(HintType hintType)
    {
        hints[hintType].ApplyTip(gameScript,resourceManager);
        hints[hintType].SetUI(hintsButtons[hintType], resourceManager);
    }
    public void SetUI()
    {
        var s = hints.Keys;
        foreach (var item in s)
        {
            hints[item].SetUI(hintsButtons[item], resourceManager);
        }
    }
}
public interface IHint
{
    void ApplyTip(GameScript gameScript, ResourceManager resourceManager);
    void SetUI(HintBehaviour hintBehaviour, ResourceManager resourceManager);
}

[Serializable]
public class DoubleTryHint : IHint
{
    public void ApplyTip(GameScript gameScript, ResourceManager resourceManager)
    {
        if(resourceManager.tipDoubleCount > 0)
        {
            gameScript.doubleTry = true;
            resourceManager.tipDoubleCount--;
            Debug.Log("Двойная попытка");
        }
    }
    public void SetUI(HintBehaviour hintBehaviour, ResourceManager resourceManager)
    {
        if(resourceManager.tipDoubleCount > 0)
        {
            hintBehaviour.countText.text = string.Format("x{0}",resourceManager.tipDoubleCount);
            hintBehaviour.button.interactable = true;
        }
        else
        {
            hintBehaviour.countText.text = string.Format("x{0}", resourceManager.tipDoubleCount);
            hintBehaviour.button.interactable = false;
        }
    }
}
[Serializable]
public class ChanceHint : IHint
{
    public void ApplyTip(GameScript gameScript, ResourceManager resourceManager)
    {
        if(resourceManager.tipChanceCount > 0)
        {
            int z = 2;
            for (int i = 0; i < gameScript.currentQuize.answers.Count; i++)
            {
                if (z > 0)
                {
                    if (!gameScript.currentQuize.answers[i].rightAnswer)
                    {
                        gameScript.Buttons[i].gameObject.GetComponent<Animation>().Blend("TurnOffButton");
                        gameScript.Buttons[i].gameObject.GetComponent<Animation>().Blend(string.Format("TurnOffText"));
                        z--;
                    }
                }
            }
            Debug.Log("50/50");
            resourceManager.tipChanceCount--;
        }
        
    }

    public void SetUI(HintBehaviour hintBehaviour, ResourceManager resourceManager)
    {
        if (resourceManager.tipChanceCount > 0)
        {
            hintBehaviour.countText.text = string.Format("x{0}", resourceManager.tipChanceCount);
            hintBehaviour.button.interactable = true;
        }
        else
        {
            hintBehaviour.countText.text = string.Format("x{0}", resourceManager.tipChanceCount);
            hintBehaviour.button.interactable = false;
        }
    }
}
[Serializable]
public class SkipQuestHint : IHint
{
    public void ApplyTip(GameScript gameScript, ResourceManager resourceManager)
    {
        if(resourceManager.tipSkipCount > 0)
        {
            gameScript.GetAnswer = true;
            Debug.Log("Пропустил вопрос");
            resourceManager.tipSkipCount--;
        }
        
    }

    public void SetUI(HintBehaviour hintBehaviour, ResourceManager resourceManager)
    {
        if (resourceManager.tipSkipCount > 0)
        {
            hintBehaviour.countText.text = string.Format("x{0}", resourceManager.tipSkipCount);
            hintBehaviour.button.interactable = true;
        }
        else
        {
            hintBehaviour.countText.text = string.Format("x{0}", resourceManager.tipSkipCount);
            hintBehaviour.button.interactable = false;
        }
    }
}
[Serializable]
public class AddTimeHint : IHint
{
    public void ApplyTip(GameScript gameScript, ResourceManager resourceManager)
    {
        if(resourceManager.tipAddTimeCount> 0)
        {
            gameScript.Time += 25f;
            Debug.Log("Добавил время");
            resourceManager.tipAddTimeCount--;
        }
       
    }

    public void SetUI(HintBehaviour hintBehaviour, ResourceManager resourceManager)
    {
        if (resourceManager.tipAddTimeCount > 0)
        {
            hintBehaviour.countText.text = string.Format("x{0}", resourceManager.tipAddTimeCount);
            hintBehaviour.button.interactable = true;
        }
        else
        {
            hintBehaviour.countText.text = string.Format("x{0}", resourceManager.tipAddTimeCount);
            hintBehaviour.button.interactable = false;
        }
    }
}